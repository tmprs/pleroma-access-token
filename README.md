# Access Token Generator for Pleroma instances

Helps generate an access token, client id, and client secret for all that API fun.

Original source code is on Github:    
https://github.com/prplecake/pleroma-access-token/